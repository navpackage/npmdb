function DB(config){
    this.connection = (function (collection, bucket) {
        var mongodb = require('mongodb');
        //var config = require('../config');
        var redisPackage = require('@navpackage/redis');
        var redisService = new redisPackage('path/to/privatekey.json');
        const Q = require('q');
        const fs = require('fs');
        const path = require('path');
        var mime = require('mime');
    
        var MongoClient = mongodb.MongoClient;
    
        var dbname = config.db_name.toLowerCase();
        var dbcollection = collection ? collection.toLowerCase() : 'INVALID';
        let bucketName = bucket;
        // ********** CONNECTION FUNCTION ************** //
        var connect = function (callback) {
    
            let coll = collection.split('.');
            if (bucket) {
                dbname = coll[0];
            } else if (coll.length === 2) {
                dbname = coll[0];
                dbcollection = coll[1].toLowerCase();
            } else {
                dbname = config.db_name;
                dbcollection = coll[coll.length - 1].toLowerCase();
            }
            redisService.get('db_uri:' + dbname).then(dbKeyValue => {
                let dbUri = config.db_uri;
                if (dbKeyValue.values.length > 0) {
                    dbUri = dbKeyValue.values;
                }
                MongoClient.connect(dbUri, { useNewUrlParser: true }, function (err, client) {
                    if (err) {
                        console.error("Error Connecting DB : ", err);
                    } else {
                        callback(client.db(dbname), client);
                    }
                });
            })
        };
    
        // ********** MAIN FUNCTION ************** //
        var count = function (filter) {
            this.mode = 'count';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
            }
            this.filter = filter;
            this.projection = {};
            return this;
        };
        var find = function (filter) {
            this.mode = 'find';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
                if (dtre2.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (dtre.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (typeof filter[attributename] === 'object') {
                    for (var arrattrname in filter[attributename]) {
                        if (dtre2.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                        if (dtre.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                    }
                }
            }
            this.filter = filter;
            this.projection = {};
            return this;
        };
        var findById = function (id) {
            this.mode = 'findById';
            this.filterId = new mongodb.ObjectID(id);
            this.projection = {};
            return this;
        };
        var findOne = function (filter) {
            this.mode = 'findOne';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
                if (dtre2.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (dtre.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (typeof filter[attributename] === 'object') {
                    for (var arrattrname in filter[attributename]) {
                        if (dtre2.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                        if (dtre.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                    }
                }
            }
            this.filter = filter;
            this.projection = {};
            return this;
        };
        var findTop = function (filter) {
            this.mode = 'findTop';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
                if (dtre2.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (dtre.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (typeof filter[attributename] === 'object') {
                    for (var arrattrname in filter[attributename]) {
                        if (dtre2.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                        if (dtre.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                    }
                }
            }
            this.filter = filter;
            this.projection = {};
            return this;
        };
        var findOneAndUpdate = function (filter, document, upsert) {
            this.mode = 'findOneAndUpdate';
    
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
            }
    
            let doc = (document.$set || document.$setOnInsert) ? (document.$set || document.$setOnInsert) : document;
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
            }
    
            this.filter = filter;
            doc.modifiedDate = new Date();
            this.updateDoc = document;
            this.upsert = upsert || false;
            this.projection = {};
            return this;
        };
    
        var insertOne = function (doc) {
            this.mode = 'insertOne';
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (!Array.isArray(doc[attributename]) && re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
                if (Array.isArray(doc[attributename])) {
                    for (var i = 0; i < doc[attributename].length; i++) {
                        if (typeof doc[attributename][i] === 'string') {
                            if (re.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i] = new mongodb.ObjectID(doc[attributename][i]);
                            }
                        }
                        else {
                            for (var arrattrname in doc[attributename][i]) {
                                if (dtre2.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (dtre.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (re.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new mongodb.ObjectID(doc[attributename][i][arrattrname]);
                                }
                            }
                        }
                    }
                }
                if (typeof doc[attributename] === 'object') {
                    for (var arrattrname in doc[attributename]) {
                        if (dtre2.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (dtre.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (re.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new mongodb.ObjectID(doc[attributename][arrattrname]);
                        }
                    }
                }
            }
    
            doc.createdDate = new Date();
            doc.modifiedDate = new Date();
            this.newDocument = doc;
            return this;
        };
        var insertMany = function (docs) {
            this.mode = 'insertMany';
            for (var i = 0; i < docs.length; i++) {
                for (var attributename in docs[i]) {
                    var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                    var dtre = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/g;
                    var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                    if (re.test(docs[i][attributename])) {
                        docs[i][attributename] = new mongodb.ObjectID(docs[i][attributename]);
                    }
                    if (dtre2.test(docs[i][attributename])) {
                        docs[i][attributename] = new Date(docs[i][attributename]);
                    }
                    if (dtre.test(docs[i][attributename])) {
                        docs[i][attributename] = new Date(docs[i][attributename]);
                    }
                    if (Array.isArray(docs[i][attributename])) {
                        for (var j = 0; j < docs[i][attributename].length; j++) {
                            for (var arrattrname in docs[i][attributename][j]) {
                                if (dtre2.test(docs[i][attributename][j][arrattrname])) {
                                    docs[i][attributename][j][arrattrname] = new Date(docs[i][attributename][j][arrattrname]);
                                }
                                if (dtre.test(docs[i][attributename][j][arrattrname])) {
                                    docs[i][attributename][j][arrattrname] = new Date(docs[i][attributename][j][arrattrname]);
                                }
                                if (re.test(docs[i][attributename][j][arrattrname])) {
                                    docs[i][attributename][j][arrattrname] = new mongodb.ObjectID(docs[i][attributename][j][arrattrname]);
                                }
                            }
                        }
                    }
                    if (typeof docs[i][attributename] === 'object') {
                        for (var arrattrname in docs[i][attributename]) {
                            if (dtre2.test(docs[i][attributename][arrattrname])) {
                                docs[i][attributename][arrattrname] = new Date(docs[i][attributename][arrattrname]);
                            }
                            if (dtre.test(docs[i][attributename][arrattrname])) {
                                docs[i][attributename][arrattrname] = new Date(docs[i][attributename][arrattrname]);
                            }
                            if (re.test(docs[i][attributename][arrattrname])) {
                                docs[i][attributename][arrattrname] = new mongodb.ObjectID(docs[i][attributename][arrattrname]);
                            }
                        }
                    }
                }
                docs[i].createdDate = new Date();
                docs[i].modifiedDate = new Date();
            }
    
            this.newDocuments = docs;
            return this;
        };
        var updateById = function (id, document) {
            this.mode = 'updateById';
    
            let doc = document.$set ? document.$set : document;
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
                if (Array.isArray(doc[attributename])) {
                    for (var i = 0; i < doc[attributename].length; i++) {
                        if (typeof doc[attributename][i] === 'object') {
                            for (var arrattrname in doc[attributename][i]) {
                                if (dtre2.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (dtre.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (re.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new mongodb.ObjectID(doc[attributename][i][arrattrname]);
                                }
                            }
                        } else if (typeof doc[attributename][i] === 'string') {
                            if (re.test(doc[attributename][i])) {
                                doc[attributename][i] = new mongodb.ObjectID(doc[attributename][i]);
                            }
                        }
                    }
                } else if (typeof doc[attributename] === 'object') {
                    for (var arrattrname in doc[attributename]) {
                        if (dtre2.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (dtre.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (re.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new mongodb.ObjectID(doc[attributename][arrattrname]);
                        }
                    }
                } else if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                } else if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                } else if (re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
            }
            delete document._id;
            if (document.$set) {
                document.$set.modifiedDate = new Date();
            } else if (document.$unset) {
                document.$set = {
                    modifiedDate: new Date()
                }
            } else {
                document.modifiedDate = new Date();
            }
            this.filterId = new mongodb.ObjectID(id);
            this.updateDoc = document;
            return this;
        }
        var updateByFilter = function (filter, document) {
            this.mode = 'updateByFilter';
    
            let doc = document.$set ? document.$set : document;
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
                if (dtre2.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (dtre.test(filter[attributename])) {
                    filter[attributename] = new Date(filter[attributename]);
                }
                if (typeof filter[attributename] === 'object') {
                    for (var arrattrname in filter[attributename]) {
                        if (dtre2.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                        if (dtre.test(filter[attributename][arrattrname])) {
                            filter[attributename][arrattrname] = new Date(filter[attributename][arrattrname]);
                        }
                    }
                }
            }
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
                if (Array.isArray(doc[attributename])) {
                    for (var i = 0; i < doc[attributename].length; i++) {
                        for (var arrattrname in doc[attributename][i]) {
                            if (dtre2.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                            }
                            if (dtre.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                            }
                            if (re.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new mongodb.ObjectID(doc[attributename][i][arrattrname]);
                            }
                        }
                    }
                }
                if (typeof doc[attributename] === 'object') {
                    for (var arrattrname in doc[attributename]) {
                        if (dtre2.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (dtre.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (re.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new mongodb.ObjectID(doc[attributename][arrattrname]);
                        }
                    }
                }
            }
            doc.modifiedDate = new Date();
    
            this.filter = filter;
            this.updateDoc = document;
            return this;
        }
        var updateMany = function (filter, document) {
            this.mode = 'updateMany';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
            }
    
            let doc = document.$set ? document.$set : document;
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
                if (Array.isArray(doc[attributename])) {
                    for (var i = 0; i < doc[attributename].length; i++) {
                        for (var arrattrname in doc[attributename][i]) {
                            if (dtre2.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new Date(doc[arrattrname]);
                            }
                            if (dtre.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                            }
                            if (re.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i][arrattrname] = new mongodb.ObjectID(doc[attributename][i][arrattrname]);
                            }
                        }
                    }
                }
                if (typeof doc[attributename] === 'object') {
                    for (var arrattrname in doc[attributename]) {
                        if (dtre2.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (dtre.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (re.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new mongodb.ObjectID(doc[attributename][arrattrname]);
                        }
                    }
                }
            }
    
            delete document._id;
            doc.modifiedDate = new Date();
    
            this.filter = filter;
            this.updateDoc = doc;
            return this;
        };
        var remove = function (filter) {
            this.mode = 'remove';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
            }
            this.filter = filter;
            return this;
        };
        var distinct = function (colname, filter) {
            this.mode = 'distinct';
            for (var attributename in filter) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                if (re.test(filter[attributename])) {
                    filter[attributename] = new mongodb.ObjectID(filter[attributename]);
                }
            }
            this.distinctColumn = colname;
            this.filter = filter || {};
            return this;
        }
        var aggregate = function (document) {
            this.mode = 'aggregate';
            this.aggregateDoc = document;
            return this;
        };
        var bulk = function (documents) {
            this.mode = 'bulk';
            this.bulkDocs = documents;
            return this;
        };
    
        // ********** GRIDFS **************//
        var upload = function (docPath, docName) {
            this.mode = 'upload';
    
            this.docPath = docPath || '';
            this.docName = docName || '';
            return this;
        }
    
        // ********** OPTIONAL SETTINGS ************** //
        var orderBy = function (order) {
            if (['find', 'findOne', 'findTop'].indexOf(this.mode) >= 0) {
                this.orderby = order;
            } else {
                this.orderby = null;
            }
            return this;
        }
        var paginate = function (page) {
            if (['find', 'findOne'].indexOf(this.mode) >= 0) {
                this.page = page
            } else {
                this.page = null;
            }
            return this;
        }
        var excludeColumns = function (arrCols) {
            if (['find', 'findById', 'findOne', 'findTop'].indexOf(this.mode) >= 0) {
                this.projection = {};
                arrCols.forEach(element => {
                    this.projection[element] = 0;
                });
    
            } else {
                this.projection = {};
            }
            return this;
        }
        var includeColumns = function (arrCols) {
            if (['find', 'findById', 'findOne', 'findTop'].indexOf(this.mode) >= 0) {
                this.projection = {};
                arrCols.forEach(element => {
                    this.projection[element] = 1;
                });
            } else {
                this.projection = {};
            }
            return this;
        }
        var upload = function (doc) {
            this.mode = 'upload';
    
            for (var attributename in doc) {
                var re = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
                var dtre = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z)/g;
                var dtre2 = /^(\d{4}-\d{2}-\d{2})/g;
    
                if (dtre2.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (dtre.test(doc[attributename])) {
                    doc[attributename] = new Date(doc[attributename]);
                }
                if (!Array.isArray(doc[attributename]) && re.test(doc[attributename])) {
                    doc[attributename] = new mongodb.ObjectID(doc[attributename]);
                }
                if (Array.isArray(doc[attributename])) {
                    for (var i = 0; i < doc[attributename].length; i++) {
                        if (typeof doc[attributename][i] === 'string') {
                            if (re.test(doc[attributename][i][arrattrname])) {
                                doc[attributename][i] = new mongodb.ObjectID(doc[attributename][i]);
                            }
                        }
                        else {
                            for (var arrattrname in doc[attributename][i]) {
                                if (dtre2.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (dtre.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new Date(doc[attributename][i][arrattrname]);
                                }
                                if (re.test(doc[attributename][i][arrattrname])) {
                                    doc[attributename][i][arrattrname] = new mongodb.ObjectID(doc[attributename][i][arrattrname]);
                                }
                            }
                        }
                    }
                }
                if (typeof doc[attributename] === 'object') {
                    for (var arrattrname in doc[attributename]) {
                        if (dtre2.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (dtre.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new Date(doc[attributename][arrattrname]);
                        }
                        if (re.test(doc[attributename][arrattrname])) {
                            doc[attributename][arrattrname] = new mongodb.ObjectID(doc[attributename][arrattrname]);
                        }
                    }
                }
            }
            doc.createdDate = new Date();
            doc.modifiedDate = new Date();
            this.newDocument = doc;
            return this;
        }
        var download = function (id, filter) {
            this.mode = 'download';
    
            this.filter = filter;
            this.filterId = id;
            return this;
        }
    
        // ********** CREATE TABLE/VIEW FUNCTION ************** //
        var checkViewCollection = function (db, viewCollection) {
            let defer = Q.defer();
            if (viewCollection.startsWith('vw_')) {
                // console.log(dbname, "DB")
                let filePath = path.join(__dirname, '../dbviews', viewCollection + '.js');
                if (fs.existsSync(filePath)) {
                    let view = require(filePath)();
                    let options = {
                        viewOn: view.viewOn,
                        pipeline: view.pipeline
                    }
                    redisService
                        .get('view:' + dbname + ':' + viewCollection)
                        .then(dbKeyValue => {
                            if (dbKeyValue.values.length === 0) {
                                console.log("Collection Not found.. creating fresh : ", viewCollection);
                                db.createCollection(viewCollection, options, () => {
                                    redisService.setnoexpiry('view:' + dbname + ':' + viewCollection, view.version);
                                    console.log("Collection created.. fresh version : ", viewCollection);
                                    defer.resolve();
                                })
                            } else if (view.version !== dbKeyValue.values) {
                                db.dropCollection(viewCollection, () => {
                                    console.log("Collection dropped.. old version : ", viewCollection);
                                    db.createCollection(viewCollection, options, () => {
                                        redisService.setnoexpiry('view:' + dbname + ':' + viewCollection, view.version);
                                        console.log("Collection recreated.. new version : ", viewCollection);
                                        defer.resolve();
                                    })
                                })
                            } else {
                                defer.resolve();
                            }
                        })
                } else {
                    // defer.reject({ msg: "VIEW file does not exists" });
                    defer.resolve();
                }
            } else {
                defer.resolve();
            }
            return defer.promise;
        }
        // ********** EXEC() FUNCTION ************** //
        const createIndex = async (keys, options) => {
            let defer = Q.defer();
            connect(async (db, client) => {
                try {
                    db.collection(dbcollection, (cErr, collection) => {
                        collection.createIndex(keys, options).then(() => {
                            defer.resolve(`${dbname}.${dbcollection}.${options.name}: Successful`)
                        }).catch(err => {
                            defer.reject(`${dbname}.${dbcollection}.${options.name}: Failed - ${err.errmsg}`);
                        });
                    });
                } catch (error) {
                    defer.reject(`${dbname}.${dbcollection}.${options.name}: Failed - ${error.errmsg}`);
                }
            });
            return defer.promise;
        }
        const dropIndex = async (indexName) => {
            let defer = Q.defer();
            connect(async (db, client) => {
                try {
                    db.collection(dbcollection, async (cErr, collection) => {
                        try {
                            if (!indexName) {
                                defer.reject(`${dbname}.${dbcollection}: Index Name Required for it to be Dropped`);
                            } else {
                                let indexExists = await collection.indexExists(indexName);
                                if (!indexExists) {
                                    defer.resolve(`${dbname}.${dbcollection}.${indexName}: Dropped Index Skipped`);
                                } else {
                                    await collection.dropIndex(indexName)
                                    defer.resolve(`${dbname}.${dbcollection}.${indexName}: Dropping Index Successful`);
                                }
                            }
                        } catch (err) {
                            console.error(err);
                            defer.reject(`${dbname}.${dbcollection}: Failed Dropping Index ${indexName}`);
                        }
                    });
                } catch (error) {
                    defer.reject(`${dbname}.${dbcollection}: Failed Dropping Index ${indexName}`);
                }
            });
            return defer.promise;
        }
        var exec = function (callback) {
            let defer = Q.defer();
            connect((db, client) => {
    
                if (bucketName) {
                    var bucket = new mongodb.GridFSBucket(db, {
                        chunkSizeBytes: 5120,
                        bucketName: bucketName
                    });
    
                    if (this.mode === 'upload') {
                        let mimetype = mime.getType(this.newDocument.path);
                        fs.createReadStream(this.newDocument.path).
                            pipe(bucket.openUploadStream(this.newDocument.name,
                                {
                                    contentType: mimetype,
                                    metadata: this.newDocument.metadata
                                }
                            )).
                            on('error', (error) => {
                                console.error(error);
                                fs.unlink(this.newDocument.path);
                                client.close();
                                if (callback) {
                                    callback(error);
                                    return;
                                } else {
                                    defer.reject(error);
                                }
                            }).
                            on('finish', (response) => {
                                console.log('done!', response);
                                client.close();
                                fs.unlink(this.newDocument.path);
                                if (callback) {
                                    callback(response);
                                } else {
                                    defer.resolve(response);
                                    // console.info(response);
                                    // return;
                                }
                            });
                    } else if (this.mode === 'download') {
                        bucket.openDownloadStream(this.filterId).
                            pipe(fs.createWriteStream(this.filter.path)).
                            on('error', (error) => {
                                console.error(error);
                                if (callback) {
                                    callback(error);
                                }
                                client.close();
                            }).
                            on('finish', (response) => {
                                console.log('done!');
                                if (callback) {
                                    callback(response);
                                } else {
                                    // console.info(response);
                                    // return;
                                    defer.resolve(response);
                                }
                                client.close();
                            });
                    } else if (this.mode === 'find') {
                        db.collection(dbcollection, (cErr, collection) => {
                            if (!this.page) {
                                this.page = {
                                    size: config.db_page_size,
                                    number: 1
                                };
                            }
                            collection.countDocuments(this.filter, (err, count) => {
                                this.page.records = count;
                                var query = collection.find(this.filter);
    
                                query.project(this.projection);
    
                                if (this.orderby) {
                                    query.sort(this.orderby);
                                }
    
                                if (this.page && this.page.size && this.page.number) {
                                    query.limit(this.page.size);
                                    query.skip(this.page.size * (this.page.number - 1));
                                }
                                query.toArray((fErr, results) => {
                                    var response = {};
                                    if (fErr) {
                                        console.error(fErr);
                                        response.err = fErr;
                                    } else {
                                        response.results = results;
                                        response.page = this.page;
                                    }
                                    client.close();
    
                                    if (callback) {
                                        callback(response);
                                    } else {
                                        defer.resolve(response);
                                    }
                                });
                            });
                        });
                    }
                } else {
                    checkViewCollection(db, dbcollection).then(() => {
                        db.collection(dbcollection, (cErr, collection) => {
                            if (this.mode === 'count') {
                                collection.countDocuments(this.filter, (err, count) => {
                                    var response = { count: 0 };
                                    if (err) {
                                        console.error(err);
                                        response.err = err;
                                    } else {
                                        response.count = count;
                                    }
                                    client.close();
                                    if (callback) {
                                        callback(response);
                                    } else {
                                        defer.resolve(response);
                                    }
                                });
    
                            } else if (this.mode === 'find') {
                                if (!this.page) {
                                    this.page = {
                                        size: config.db_page_size,
                                        number: 1
                                    };
                                }
                                collection.countDocuments(this.filter, (err, count) => {
                                    this.page.records = count;
                                    var query = collection.find(this.filter);
    
                                    query.project(this.projection);
    
                                    if (this.orderby) {
                                        query.sort(this.orderby);
                                    }
    
                                    if (this.page && this.page.size && this.page.number) {
                                        query.limit(this.page.size);
                                        query.skip(this.page.size * (this.page.number - 1));
                                    }
                                    query.toArray((fErr, results) => {
                                        var response = {};
                                        if (fErr) {
                                            console.error(fErr);
                                            response.err = fErr;
                                        } else {
                                            response.results = results;
                                            response.page = this.page;
                                        }
                                        client.close();
    
                                        if (callback) {
                                            callback(response);
                                        } else {
                                            defer.resolve(response);
                                        }
                                    });
                                });
                            } else if (this.mode === 'findById') {
                                let options = {};
                                if(this.projection) options.projection = this.projection;
                                collection.findOne({ _id: this.filterId }, options, (fErr, result) => {
                                    if (fErr) {
                                        console.error(fErr);
                                        var response = {
                                            err: fErr
                                        };
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                    } else {
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                    client.close();
                                });
                            } else if (this.mode === 'findOne') {
                                let options = {};
                                if(this.projection) options.projection = this.projection;
                                if(this.orderby) options.sort = this.orderby;
    
                                collection.findOne(this.filter, options, (fErr, result) => {
                                    if (fErr) {
                                        console.error(fErr);
                                        var response = {
                                            err: fErr
                                        };
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                        client.close();
                                    } else {
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                        client.close();
                                    }
                                });
                            } else if (this.mode === 'findTop') {
                                collection.countDocuments(this.filter, (err, count) => {
                                    var query = collection.find(this.filter);
    
                                    query.project(this.projection);
                                    if (this.orderby) {
                                        query.sort(this.orderby);
                                    }
                                    query.limit(1);
                                    query.skip(0);
                                    query.toArray((fErr, results) => {
                                        var response = {};
                                        if (fErr) {
                                            console.error(fErr);
                                            response.err = fErr;
                                        } else if (results.length > 0) {
                                            response = results[0];
                                        } else {
                                            response = null;
                                        }
                                        client.close();
    
                                        if (callback) {
                                            callback(response);
                                        } else {
                                            defer.resolve(response);
                                        }
                                    });
                                });
                            } else if (this.mode === 'insertOne') {
                                collection.insertOne(this.newDocument, (iErr, result) => {
                                    if (iErr) {
                                        var response = {
                                            err: iErr,
                                            result: {},
                                            data: {}
                                        };
                                        client.close();
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                    } else {
                                        client.close();
                                        if (callback) callback(result.ops[0]);
                                        if (!callback) defer.resolve(result.ops[0]);
                                    }
                                });
                            } else if (this.mode === 'insertMany') {
                                collection.insertMany(this.newDocuments, { ordered: false }, (iErr, result) => {
                                    if (iErr) {
                                        var response = {
                                            err: iErr,
                                            result: {},
                                            data: {}
                                        };
                                        client.close();
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                    } else {
                                        client.close();
                                        if (callback) callback(result.ops[0]);
                                        if (!callback) defer.resolve(result.ops[0]);
                                    }
                                });
                            } else if (this.mode === 'updateById') {
                                collection.update({
                                    "_id": this.filterId
                                }, this.updateDoc, (uErr, result) => {
                                    if (uErr) {
                                        console.error(uErr);
                                        client.close();
                                        if (callback) callback(uErr);
                                        if (!callback) defer.resolve(uErr);
                                    } else {
                                        client.close();
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                });
                            } else if (this.mode === 'updateByFilter') {
                                collection.update(this.filter, this.updateDoc, (uErr, result) => {
                                    if (uErr) {
                                        console.error(uErr);
                                        client.close();
                                        if (callback) callback(uErr);
                                        if (!callback) defer.resolve(uErr);
                                    } else {
                                        client.close();
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                });
                            } else if (this.mode === 'updateMany') {
                                collection.updateMany(this.filter, { $set: this.updateDoc }, (uErr, result) => {
                                    if (uErr) {
                                        console.error(uErr);
                                        client.close();
                                        if (callback) callback(uErr);
                                        if (!callback) defer.resolve(uErr);
                                    } else {
                                        client.close();
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                });
                            } else if (this.mode === 'findOneAndUpdate') {
                                collection.findOneAndUpdate(this.filter, this.updateDoc, { upsert: this.upsert }, (uErr, result) => {
                                    if (uErr) {
                                        console.error(uErr);
                                        client.close();
                                        if (callback) callback(uErr);
                                        if (!callback) defer.resolve(uErr);
                                    } else {
                                        client.close();
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                });
                            } else if (this.mode === 'remove') {
                                collection.remove(this.filter, (fErr, result) => {
                                    if (fErr) {
                                        console.error(fErr);
                                        var response = {
                                            err: fErr
                                        };
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                    } else {
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                    client.close();
                                });
                            } else if (this.mode === 'distinct') {
                                collection.distinct(this.distinctColumn, this.filter, (fErr, result) => {
                                    if (fErr) {
                                        console.error(fErr);
                                        var response = {
                                            err: fErr
                                        };
                                        if (callback) callback(response);
                                        if (!callback) defer.resolve(response);
                                    } else {
                                        if (callback) callback(result);
                                        if (!callback) defer.resolve(result);
                                    }
                                    client.close();
                                });
                            } else if (this.mode === 'aggregate') {
                                var cursor = collection.aggregate(
                                    this.aggregateDoc,
                                    { allowDiskUse: true },
                                    { cursor: { batchSize: 900 } }
                                );
                                var response = {
                                    err: null,
                                    results: []
                                };
                                cursor.toArray((fErr, results) => {
                                    if (fErr) {
                                        console.error(fErr);
                                        response.err = fErr;
                                    } else if (results) {
                                        response.results = results;
                                    }
                                    client.close();
                                    if (callback) callback(response);
                                    if (!callback) defer.resolve(response);
                                });
                            } else if (this.mode === 'bulk') {
                                collection.bulkWrite(
                                    this.bulkDocs, { ordered: false }, (iErr, result) => {
                                        if (iErr) {
                                            var response = {
                                                err: iErr,
                                                result: {},
                                                data: {}
                                            };
                                            client.close();
                                            if (callback) callback(response);
                                            if (!callback) defer.resolve(response);
                                        } else {
                                            client.close();
                                            if (callback) callback(result);
                                            if (!callback) defer.resolve(result);
                                        }
                                    });
                            } else {
                                if (callback) {
                                    callback();
                                } else {
                                    defer.resolve();
                                }
                            }
                        });
                    });
                }
            });
            if (!callback)
                return defer.promise;
        }
    
        // ********** PRIVATE FUNCTION ************** //
        var convertToMongoId = function (strId) {
            return new mongodb.ObjectID(strId);
        };
        const dropCollection = async () => {
            let defer = Q.defer();
            connect(async (db, client) => {
                try {
                    if (!dbcollection) {
                        defer.reject(`${dbname}: Collection Name Required for it to be Dropped`);
                    } else {
                        db.dropCollection(dbcollection, () => {
                            console.log("Temp Collection dropped.....: ", dbcollection);
                            defer.resolve(`${dbname}.${dbcollection}: Dropping Collection Successful`);
                        })
                    }
                } catch (error) {
                    defer.reject(`${dbname}.${dbcollection}: Failed Dropping Collection ${dbcollection}`);
                }
            });
            return defer.promise;
        }
    
        return {
            count: count,
            find: find,
            findById: findById,
            findOne: findOne,
            findTop: findTop,
            findOneAndUpdate: findOneAndUpdate,
            insertOne: insertOne,
            insertMany: insertMany,
            updateById: updateById,
            updateByFilter: updateByFilter,
            updateMany: updateMany,
            remove: remove,
            aggregate: aggregate,
            bulk: bulk,
            distinct: distinct,
            paginate: paginate,
            orderBy: orderBy,
            excludeColumns: excludeColumns,
            includeColumns: includeColumns,
            upload: upload,
            download: download,
    
            createIndex,
            dropIndex,
    
            exec: exec,
            mongoId: convertToMongoId,
            dropCollection
        };
    });
}
module.exports = DB;
